/*
  Morse.cpp - Library for flashing Morse code.
  Created by David A. Mellis, November 2, 2007.
  Released into the public domain.
*/

#include "Arduino.h"
#include "TimerG.h"

TimerG::TimerG()
{
    tant = millis();
    iniciado = false;
}

bool TimerG::begin() {   //Comienza el contador
    tant = millis();
    iniciado = true;
    return false;
}

bool TimerG::status(int tiempo, bool Auto) {  //Si auto es true la bandera se reinicia sino espera a un nuevo begin
    if (millis() - tant > tiempo && iniciado) {
        if (Auto) tant = millis();
        else iniciado = false;
        cont++;
        return true;
    }
    if (millis() < tant)tant = millis();
    return false;
}

int TimerG::time() {
    if (!iniciado)return 0;
    else if (millis() > tant)        return millis() - tant;
    else return 0;
}


