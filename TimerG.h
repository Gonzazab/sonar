#ifndef TimerG_h
#define TimerG_h

#include "Arduino.h"

class TimerG
{
  public:
      TimerG();   //Inicializa el contador   
      bool begin();  //Comienza el contador retorna true si ya estaba iniciado
      bool status(int tiempo, bool Auto = false); //Si auto es true la bandera se reinicia sino espera a un nuevo begin
      int time();               //Retorna la cantidad de ms desde el begin
      int cont;               //Contador de intentos
  private:
      unsigned long tant;
      bool iniciado;
};

#endif
