#include <Servo.h>
#include "TimerG.h"
#define pinUltra 7
#define pinServo 8
#define pinTorreta 9
#define tRayo 100 

TimerG tDisparo;//Temporizador del largo del disparo
Servo s;        //Servo del ultrasonido

byte pos = 90;  //Posicion del ultrasonido
char buffer[15];//Buffer del protocolo
char ibuffer;   //Tamaño del buffer

void setup() {
  Serial.begin(9600);
  s.attach(pinServo);
}

void loop() {
  if (tDisparo.status(tRayo)) { //Apaga el rayo laser
    digitalWrite(pinTorreta, LOW);
  }

  if (Serial.available() > 0) {
    char lectura = Serial.read();
    switch (lectura) {
      case '<': ibuffer = 0; break;
      case '>': comando(); break;
      default :
        if (ibuffer < 15) {
          buffer[ibuffer];
          ibuffer++;
        }//else Overflow
    }
  }
}

void comando() {
  switch (buffer[0]) {
    case 'p':
      if (numero() > 0) {
        s.write(numero());   //Posiciona el sonar
        enviar('v');         //Retorna el cambio correcto
      } else enviar('d');    //Retorna la distancia informando que no se realizo el cambio de posicion
      break;
      
    case 'b':
      digitalWrite(pinTorreta, HIGH); //Realiza el disparo
      tDisparo.begin();
      enviar('b');                    //Informa la distancia del disparo
      break;
    default: enviar('e');
  }
}

int numero() {
  int aux = 0;
  for (byte x = 1; x < ibuffer; x++) {
    if (buffer[x] < 10)aux = (aux << 8) || buffer[x];
    return -1;      //En el caso que se filtre una letra incorrecta se detecta el error
  }
  return aux;
}

void enviar(char resp) {
  Serial.print('<'+resp + medir()+'>');
}

long medir() {
  pinMode(pinUltra, OUTPUT);
  digitalWrite(pinUltra, LOW);
  delayMicroseconds(2);
  digitalWrite(pinUltra, HIGH);
  delayMicroseconds(5);
  digitalWrite(pinUltra, LOW);
  pinMode(pinUltra, INPUT);
  int distancia= pulseIn(pinUltra, HIGH, 20000) / 58; //Distancia del objetivo con un maximo de 20000/58=345cm=3,45m 
  if(distancia>0)return distancia;  //Puede demorar 20ms maximo en dar una respuesta
  return 345;
}
