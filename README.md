# README #

Este es un sonar realizado con Arduino y controlado por comunicacion Serial desde una aplicacion realizada por Python.

### Descriptivo ###

* El Arduino funciona unicamente como un controlador de persifericos y la aplicacion de Python es el cerebro
* Proyecto para la materia Control Micropocesado del IFTS N°14 - 2021

### Comandos interpretador por el Arduino ###

* <*pxxx*> Retorna la distancia en esa posicion
* <*bxxx*> Realiza un disparo y retorna la distancia

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact